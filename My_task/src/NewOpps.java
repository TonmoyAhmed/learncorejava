class FirstOne{
    void methodOne(){
        System.out.println("This is method one");
    }
}
class SecondOne extends FirstOne{

    void methodTwo(){
        super.methodOne();
        System.out.println("This is method two");
    }
}

public class NewOpps {
    public static void main(String[] args) {
        SecondOne obj = new SecondOne();
        //obj.methodOne();
        obj.methodTwo();

    }
}
